import requests
from fastapi import FastAPI, Response, status
from fastapi.responses import RedirectResponse
from pydantic_settings import BaseSettings


class Settings(BaseSettings):
    s3_url: str = "https://minio.gnome.org/gnome-build-meta"
    cdn_url: str = "https://1270333429.rsc.cdn77.org"
    mirror_url: str = "https://download.gnome.org/gnomeos"
    main_url: str = "https://os.gnome.org"


cfg = Settings()
app = FastAPI()


@app.get("/status")
def healthcheck():
    return {"status": "OK"}


@app.get("/latest/{filename}")
def get_nightly(filename: str):
    # "Wildcard" matches based on extension, e.g. os.gnome.org links to:
    # <https://os.gnome.org/download/latest/gnome_os_installer.iso>
    if filename.endswith(".iso"):
        latest_filename = "latest-iso"
    elif filename.endswith("aarch64.img.xz"):
        latest_filename = "latest-aarch64-disk"
    elif filename.endswith("x86_64.img.xz") or  filename.endswith(".img.xz"):
        latest_filename = "latest-x86_64-disk"
    else:
        return Response(status_code=status.HTTP_404_NOT_FOUND)

    r = requests.get(f"{cfg.s3_url}/{latest_filename}")
    latest = r.text.rstrip()
    _, version, filename = latest.split("/")

    response = RedirectResponse(f"{cfg.main_url}/download/{version}/{filename}")
    response.status_code = status.HTTP_302_FOUND
    return response


@app.get("/stable/{branch}/{filename}")
def get_stable(branch: str, filename: str):
    if filename == "gnome_os_installer_x86_64.iso":
        stable_filename = f"stable-{branch}-iso"
    elif (filename == "disk_x86_64.img.xz") or (filename == "disk.img.xz"):
        stable_filename = f"stable-{branch}-x86_64-disk"
    elif filename == "disk_aarch64.img.xz":
        stable_filename = f"stable-{branch}-aarch64-disk"
    else:
        return Response(status_code=status.HTTP_404_NOT_FOUND)

    try:
        r = requests.get(f"{cfg.s3_url}/{stable_filename}")
        r.raise_for_status()
    except requests.exceptions.RequestException:
        return Response(status_code=status.HTTP_404_NOT_FOUND)

    reference = r.text.rstrip()
    _, tag, filename = reference.split("/")

    response = RedirectResponse(f"{cfg.main_url}/download/{tag}/{filename}")
    response.status_code = status.HTTP_302_FOUND
    return response


@app.get("/{version}/{filename}")
def get_versioned(version: str, filename: str):
    r = requests.head(f"{cfg.mirror_url}/{version}/{filename}")
    if r.ok:
        response = RedirectResponse(f"{cfg.mirror_url}/{version}/{filename}")
        response.status_code = status.HTTP_302_FOUND
        return response

    if version.startswith("mr"):
        folder = "merge_request"
    elif "." in version:
        folder = "tag"
    else:
        folder = "nightly"

    response = RedirectResponse(f"{cfg.cdn_url}/{folder}/{version}/{filename}")
    response.status_code = status.HTTP_302_FOUND
    return response
