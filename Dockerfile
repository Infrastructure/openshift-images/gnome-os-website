FROM registry.access.redhat.com/ubi8/python-312:1

EXPOSE 8000
ENV PYTHONPATH=/opt/app-root/src

RUN pip install --no-cache-dir fastapi uvicorn requests pydantic-settings
ADD main.py /opt/app-root/src

CMD ["uvicorn", "main:app", "--host", "0.0.0.0", "--port", "8000"]
